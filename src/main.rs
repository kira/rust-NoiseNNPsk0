use hex_literal::hex;
use std::io::{Write, Read};
use std::env;

fn show(bs: &[u8]) -> String {
    String::from_utf8_lossy(bs).into_owned()
}

pub fn main() {
    static PSK : [u8; 32] =
        hex!("324eee92611cd877841c4de9fd5253e9dba6033329a837ee5f01beb005dffb2f");
    static PATTERN: &'static str = "Noise_NNpsk0_25519_ChaChaPoly_BLAKE2b";

    let args : Vec<String> = env::args().collect();
    if args.len() < 2 {
        panic!("ERROR: program requires 1st argument to be \"true\" or \"false\"");
    }
    let initiator = match args[1].as_str() {
        "true" => true,
        "false" => false,
        _ => panic!("ERROR: program requires 1st argument to be \"true\" or \"false\"")
    };

    let mut payload = [0_u8; 48];
    let mut msg = [0_u8; 48];

    if initiator {
        // Handshake
        let mut initiator = snow::Builder::new(PATTERN.parse().unwrap())
            .prologue(b"CABLE")
            .psk(0, &PSK)
            .build_initiator()
            .unwrap();

        let len = initiator.write_message(&[], &mut msg).unwrap();
        std::io::stdout().write_all(&msg).unwrap();
        std::io::stdout().flush().unwrap();
        eprintln!("I: sent 1 (len={len})");

        std::io::stdin().read_exact(&mut payload).unwrap();
        initiator.read_message(&payload, &mut msg).unwrap();
        eprintln!("I: read 2");

        let mut initiator = initiator.into_transport_mode().unwrap();

        // Send a real msg!
        let text = b"Hello secret world, today is fun";
        msg[..text.len()].copy_from_slice(text);
        let len = initiator.write_message(&msg[..text.len()], &mut payload).unwrap();
        std::io::stdout().write_all(&payload).unwrap();
        std::io::stdout().flush().unwrap();
        eprintln!("I: sent 3 ({len})");
        eprintln!("ENCRYPTED: {:#?}", show(&payload));

        std::io::stdin().read_exact(&mut payload).unwrap();
    } else {
        // Handshake
        let mut responder = snow::Builder::new(PATTERN.parse().unwrap())
            .prologue(b"CABLE")
            .psk(0, &PSK)
            .build_responder()
            .unwrap();

        std::io::stdin().read_exact(&mut payload).unwrap();
        responder.read_message(&payload, &mut msg).unwrap();
        eprintln!("R: read 1");

        let len = responder.write_message(&[], &mut msg).unwrap();
        std::io::stdout().write_all(&msg).unwrap();
        std::io::stdout().flush().unwrap();
        eprintln!("R: sent 2 (len={len})");

        let mut responder = responder.into_transport_mode().unwrap();

        // Recv a real msg!
        std::io::stdin().read_exact(&mut payload).unwrap();
        responder.read_message(&payload, &mut msg).unwrap();
        eprintln!("R: read 3");
        eprintln!("DECRYPTED: {:#?}", show(&msg[..32]));
    }
}

